import React from "react";

class LuckyDraw extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            ball: 0,
            ball1: 0,
            ball2: 0,
            ball3: 0,
            ball4: 0,
            ball5: 0
        }
    }

    onBtnGenerate = () => {
        this.setState({
            ball: Math.floor(Math.random()*45) + 1,
            ball1: Math.floor(Math.random()*45) + 1,
            ball2: Math.floor(Math.random()*45) + 1,
            ball3: Math.floor(Math.random()*45) + 1,
            ball4: Math.floor(Math.random()*45) + 1,
            ball5: Math.floor(Math.random()*45) + 1
        })
    }
    render(){
        return(
            <div className="container text-center jombotron body">
                <h3 className = "m-5 "><b>Lucky Draw</b></h3>
                <div className="col-12  text-center d-flex justify-content-center ball">
                        <p className="styleball d-flex justify-content-center ball col-2"><b>{this.state.ball}</b></p>
                        <p className="styleball d-flex justify-content-center ball col-2"><b>{this.state.ball1}</b></p>
                        <p className="styleball d-flex justify-content-center ball col-2"><b>{this.state.ball2}</b></p>
                        <p className="styleball d-flex justify-content-center ball col-2"><b>{this.state.ball3}</b></p>
                        <p className="styleball d-flex justify-content-center ball col-2"><b>{this.state.ball4}</b></p>
                        <p className="styleball d-flex justify-content-center ball col-2"><b>{this.state.ball5}</b></p>
                </div>
                <button className="btn btn-primary m-5 " onClick={this.onBtnGenerate}>Generate</button>
            </div>
        )
    };
}
export default LuckyDraw;
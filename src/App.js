import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import LuckyDraw from './components/LuckyDraw';

function App() {
  return (
    <div >
      <LuckyDraw/>
    </div>
  );
}

export default App;
